#!/usr/bin/env python2
#-*- coding: UTF-8 -*-

# autore: carmelo.pellegrino@bo.infn.it


def read_all_file(filename):
  f = open(filename,'r')
  data = f.read()
  f.close()
  return data


def read_and_parse_affiliations(filename):
  raw_data = read_all_file(filename)
  enti = raw_data.split('\\\\')

  affiliations = {}

  for ente in enti:
    try:
      dati = ente[10:].split('}$}')
      affiliations[dati[0]] = dati[1].replace('\n', ' ')
    except:
      pass

  return affiliations

def read_and_parse_people(filename):
  raw_data = read_all_file(filename)
  persone = raw_data.split("$")

  n = len(persone) - 1

  i = 0

  people = {}

  while i < n:
    nome = persone[i].replace('\n','').replace(',','')
    affiliazioni = persone[i + 1].replace('{','').replace('}','').replace('^','').split(',')
    people[nome] = (i/2, affiliazioni)
    i += 2

  return people

def compile_author_list(people, affiliations):
  # Formato AIP
  #\author{NOME}{
  #address={Indirizzo principale}
  #,altaddress={Eventuale secondo indirizzo}
  #}

  array = {}

  for person in people:
    entry = "\\author{" + person + "}{\naddress={" + affiliations[people[person][1][0]] + "}\n"
    if len(people[person][1]) > 1:
      for i in range(len(people[person][1]) - 1):
        entry += ",altaddress={" + affiliations[people[person][1][i + 1]] + "}\n"
    entry += "}\n"

    array[people[person][0]] = entry

  keys = array.keys()

  keys.sort()

  for i in keys:
    print array[i]


aff = read_and_parse_affiliations("affiliazioni.txt")
peo = read_and_parse_people("autori.txt")

compile_author_list(peo, aff)
